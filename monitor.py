# -*- coding: utf-8 -*-
"""
VTC Trade-Monitor

"""

import urllib.request, json
import time

start_time = time.gmtime()

print("Good morning, Stefan!")
print(time.strftime("%H: %M - %d/%m", start_time))

def BTCprice():
    with urllib.request.urlopen("https://bittrex.com/api/v1.1/public/getticker?market=usdt-btc") as url:
        BTCprice = json.loads(url.read().decode())
        BTCprice = BTCprice["result"]["Last"]
    return BTCprice

def VTCprice():
    with urllib.request.urlopen("https://bittrex.com/api/v1.1/public/getticker?market=btc-vtc") as url:
        VTCprice = json.loads(url.read().decode())
        VTCprice = VTCprice["result"]["Last"]
    return VTCprice

def VTCpriceUSD():
    VTCpriceUSD = VTCprice() * BTCprice()
    return VTCpriceUSD

print("Bitcoin is currently at: %s$" % (BTCprice()))
print("Vertcoin is currenty at: %s / %s$" % (VTCprice(), VTCpriceUSD()))

VTCamount = float(input("Enter a Vertcoin(VTC) amount to start monitoring trade:"))
openprice = float(input("What was your price(BTC):"))
targetprice = float(input("Target:"))
stoploss = float(input("Stop-loss:"))

startdollarvalue = VTCamount * openprice * BTCprice()
runtime = True
print("")
print("Current position:")
while True:
    currentdollarvalue = VTCamount * VTCpriceUSD()
    change = currentdollarvalue / startdollarvalue
    percentagechange = "{:.1%}".format(change)
    profit = currentdollarvalue - startdollarvalue
    print("Current: %s for %s$ / Change: %s / Profit: %s$" % (VTCprice(), currentdollarvalue, percentagechange, profit), end="\r")
    time.sleep(60)




    
